import PropTypes from "prop-types";
import "./Header.css";

const Header = props => {
    return (
        <div className="header">
            <div className="currenciesCompare">
                UAH | USD | EUR
            </div>
            <div className="currentRate">
                1 | {props.USDtoUAH} | {props.EURtoUAH}
            </div>
        </div>
    )
}

Header.propTypes = {
    USDtoUAH: PropTypes.number,
    EURtoUAH: PropTypes.number,
};

export default Header
