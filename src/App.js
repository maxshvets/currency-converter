import { useState, useEffect } from 'react';
import axios from 'axios';
import './App.css';
import CurrencyInput from './CurrencyInput/CurrencyInput';
import Header from './Header/Header';

function App() {

  const [amount1, setAmout1] = useState(1);
  const [amount2, setAmout2] = useState(1);
  const [currency1, setCurrency1] = useState("USD");
  const [currency2, setCurrency2] = useState("UAH");
  const [rates, setRates] = useState([]);

  const apiConfig = {
    headers: {
      "apikey": "SUuxRjllox8avysd1ovZMRMrbl8w6aY7" // obviously should be env var
    }
  };

  const apiUrl = "https://api.apilayer.com/exchangerates_data/latest?symbols=&base=UAH";

  useEffect(() => {
    axios.get(apiUrl, apiConfig)
      .then(response => {
        setRates(response.data.rates);
      })
  }, []);

  const amountFormat = num => {
    return num.toFixed(4);
  }

  useEffect(() => {
    if (!!rates) {
      handleAmount1Change(1);
    }
  }, [rates]);

  const handleAmount1Change = amount1 => {
    setAmout2(amountFormat(amount1 * rates[currency2] / rates[currency1]));
    setAmout1(amount1);
  }

  const handleCurrency1Change = currency1 => {
    setAmout1(amountFormat(amount2 * rates[currency1] / rates[currency2]));
    setCurrency1(currency1);
  }

  const handleAmount2Change = amount2 => {
    setAmout1(amountFormat(amount2 * rates[currency1] / rates[currency2]));
    setAmout2(amount2);
  }

  const handleCurrency2Change = currency2 => {
    setAmout2(amountFormat(amount1 * rates[currency2] / rates[currency1]));
    setCurrency2(currency2);
  }

  return (
    <div>
      <Header 
      USDtoUAH={amountFormat(rates["USD"])}
      EURtoUAH={amountFormat(rates["EUR"])}/>
      <h1>Currency Converter</h1>
      <div className="inputContainer">
        <CurrencyInput
          currencies={Object.keys(rates)}
          amount={amount1}
          currency={currency1}
          onAmountChange={handleAmount1Change}
          onCurrencyChange={handleCurrency1Change} />
        <CurrencyInput
          currencies={Object.keys(rates)}
          amount={amount2}
          currency={currency2}
          onAmountChange={handleAmount2Change}
          onCurrencyChange={handleCurrency2Change} />
      </div>
    </div>
  );
}

export default App;
